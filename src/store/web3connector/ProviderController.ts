import * as providers from "./providers"
import { provider as Web3Provider } from "web3-core"
import { INJECTED_PROVIDER_ID, CACHED_PROVIDER_KEY } from "./constants/keys"
import { InjectedVerifier } from "./InjectedVerifier"
import { isMobile } from "./helpers"

export type ProviderControllerOptions = {
  disableInjectedProvider: boolean
  cacheProvider: boolean
  providerOptions: {
    [id: string]: any
  }
}

export class ProviderController extends EventTarget {
  private injectedVerifier = new InjectedVerifier()
  private cachedProvider = localStorage.getItem(CACHED_PROVIDER_KEY) || ""
  private injectedProvider = this.injectedVerifier.getInjectedProvider()
  private providers = providers

  private events: { [event: string]: (event: Event) => void } = {}

  constructor(private options: ProviderControllerOptions) {
    super()
  }

  on(event: string, cb: (evt: Event) => void) {
    if (this.events[event] !== undefined) return
    this.events[event] = cb
    this.addEventListener(event, this.events[event])
  }

  removeEvent(event: string) {
    this.removeEventListener(event, this.events[event])
    delete this.events[event]
  }

  private shouldDisplayProvider(id: string) {
    const provider = Object.values(this.providers).find((p) => p.id === id)
    if (provider === undefined) return false
    const providerOptions = this.options.providerOptions[id]
    if (providerOptions === undefined) return false
    const requiredOptions = provider.package?.required
    if (requiredOptions === undefined || (requiredOptions && requiredOptions.length === 0)) return true
    for (let requiredOption in requiredOptions) {
      if (!Object.prototype.hasOwnProperty.call(providerOptions, requiredOption)) return false
    }
    return true
  }

  getDisplayProviders() {
    const mobile = isMobile()

    const displayInjected = !!this.injectedProvider && !this.options.disableInjectedProvider
    const onlyInjected = displayInjected && mobile

    if (onlyInjected) return [this.injectedProvider]

    const providersIds: Array<string> = []

    Object.values(this.providers).map((provider) => {
      if (provider.id === INJECTED_PROVIDER_ID) return
      if (this.shouldDisplayProvider(provider.id)) providersIds.push(provider.id)
    })

    const displayProviders = providersIds
      .map((id) => {
        const provider = Object.values(this.providers).find((p) => p.id !== "injected" && p.id === id)
        if (provider === undefined) return
        return provider
      })
      .filter((i) => i !== undefined)
    if (displayInjected) displayProviders.push(this.injectedProvider)
    return displayProviders
  }

  clearCachedProvider() {
    this.cachedProvider = ""
    localStorage.removeItem(CACHED_PROVIDER_KEY)
  }

  setCachedProvider(id: string) {
    this.cachedProvider = id
    localStorage.setItem(CACHED_PROVIDER_KEY, id)
  }

  async connectTo(id: string) {
    try {
      const provider = Object.values(this.providers).find((p) => p.id === id)
      if (provider === undefined)
        return this.dispatchEvent(
          new CustomEvent("error", { detail: `Provider with specified id [${id}] not founded` })
        )
      const typedProvider = await provider.connector(this.options.providerOptions[id])
      this.dispatchEvent(new CustomEvent<typeof typedProvider>("connect", { detail: typedProvider }))
      if (this.options.cacheProvider && this.cachedProvider !== id) {
        this.setCachedProvider(id)
      }
    } catch (e: any) {
      this.dispatchEvent(new CustomEvent("error", { detail: String(e.toString()) }))
    }
  }

  async connectToCached() {
    const provider = Object.values(this.providers).find((p) => p.id === this.cachedProvider)
    if (provider === undefined) {
      this.dispatchEvent(
        new CustomEvent("error", { detail: `Cached provider with specified id [${this.cachedProvider}] not founded` })
      )
      return
    }
    await this.connectTo(provider.id)
  }
}
