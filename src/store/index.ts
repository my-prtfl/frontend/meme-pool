import Vue from "vue"
import Vuex from "vuex"
import { Web3Module } from "./module/Web3Module"
import { UserModule } from "./module/UserModule"
import { PoolModule } from "./module/PoolModule"
import { TokenModule } from "./module/TokenModule"
import { PoolManagerModule } from "./module/PoolManagerModule"
import { InchModule } from "./module/InchModule"
import { GeneralModule } from "./module/GeneralModule"

Vue.use(Vuex)

export default new Vuex.Store({
  state: {},
  getters: {
    menu() {
      return ($t: Function) => [
				{ to: { name: "Homepage" }, text: $t("home") },
				{ 
					to: "/about", text: $t("about"),
					items: [
						{ to: { name: "About" }, text: $t("what_is") },
						{ to: { name: "GameFi" }, text: $t("game_fi") },
						{ to: { name: "Staking" }, text: $t("staking") },
						{ to: { name: "CreatePools" }, text: $t("pool") },
						{ to: { name: "Technology" }, text: $t("technology") },
						{ to: { name: "RoadMap" }, text: $t("roadmap") },
						{ to: { name: "Partners" }, text: $t("materials_for_partners") },
						{ to: { name: "Mission" }, text: $t("mission_and_team") },
						{ to: { name: "Instructions" }, text: $t("instructions") },
						{ to: { name: "Tokenomic" }, text: $t("tokenomic") },
						{ to: { name: "Team" }, text: $t("team") }
					]
				},
				{ to: { name: "Homepage", hash: "#faq" }, text: $t("faq") }
			]
    }
  },
  mutations: {
  },
  actions: {},
  modules: {
    Web3Module: Web3Module.Web3Module,
    UserModule: UserModule.UserModule,
    TokenModule: TokenModule.TokenModule,
    PoolModule: PoolModule.PoolModule,
    PoolManagerModule: PoolManagerModule.PoolManager,
    InchModule: InchModule.InchModule,
    GeneralModule: GeneralModule.GeneralModule
  },
})
