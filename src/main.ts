import "@/assets/scss/layout.scss"

import Vue from 'vue'

import App from '@/App.vue'

import router from '@/router'
import store from '@/store'
import i18n from "@/i18n"

import VueNotification from "vue-notification"
Vue.use(VueNotification)

import VueLazyload from "vue-lazyload"
Vue.use(VueLazyload)

import VueClipboard from "vue-clipboard2"
Vue.use(VueClipboard)

Vue.config.productionTip = false

Vue.component("b-button", () => import("@/components/Button/Button.vue"))
Vue.component("r-button", () => import("@/components/Button/ButtonRounded.vue"))
Vue.component("buttons", () => import("@/components/Button/Buttons.vue"))
Vue.component("container", () => import("@/components/Container.vue"))
Vue.component("title-bar", () => import("@/components/TitleBar.vue"))

import MainMixin from "./mixins/Main"
Vue.mixin(MainMixin)

new Vue({
  router,
  store,
  i18n,
  render: h => h(App)
}).$mount('#app')
