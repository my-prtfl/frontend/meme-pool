import BigNumber from "bignumber.js"
import { VuexModule, Module, Mutation, Action } from "vuex-module-decorators"
import { gatewayUrl } from "./constants"
import { Stake } from "./sharedTypes"

export namespace PoolModule {
  export type Pool = {
    id: number
    address: string
    owner: string
    poolIndex: number
    createdAt: string

    options?: PoolOptions
    stats?: PoolStats
    stakes?: Array<Stake>
    roles?: Array<PoolRoleRegistry>
  }

  export type PoolMeta = {
    id: number
    poolId: number
    name: string
    site: string
  }

  export type PoolOptions = {
    id: number
    poolId: number
    poolAccount: string
    name: string
    site: string
    minimalTokensForStake: string
    levels: Array<number>
    poolRewardPercent: number
    lastUpdate: Date
  }

  export type PoolStats = {
    id: number
    poolId: number
    members: number
    totalStakes: number
    totalActiveStakes: number
    totalStakesSum: string
    totalActiveStakesSum: string
    totalClaims: number
    totalClaimsSum: string
  }

  export type PoolRoleRegistry = {
    id: number
    poolId: number
    name: string
    contractPermissions: Array<string>
    viewPermissions: Array<string>
  }

  export type PoolXPiRefLevel = {
    minStakesAmount: BigNumber
    minReferrals: number
    minReferralsStakesAmount: BigNumber
    depth: number
  }

  @Module({ namespaced: true })
  export class PoolModule extends VuexModule {
    private module = "PoolModule"

    $memePool: Pool = {
      id: 0,
      address: "0x" + "0".repeat(40),
      owner: "0x" + "0".repeat(40),
      poolIndex: -1,
      createdAt: "",
    }


    get memePool() {
      return this.$memePool
    }

    @Mutation
    setMemePool(pool: Pool) {
      this.$memePool = pool
    }

    @Action
    async getMemePool() {
      const method = "get/pool"
      try {
        const response = await fetch(`${gatewayUrl}/${method}`, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({ id: 2 })
        })
        this.context.commit("setMemePool", await response.json())
      } catch(e) {
        console.log(`[${this.module}] Error at [getMemePool]`)
        console.log(e)
        return null
      }
    }

    @Action
    async getPools() {
      const method = "get/pools"
      try {
        const response = await fetch(`${gatewayUrl}/${method}`, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
        })
        this.context.commit("setPools", await response.json())
      } catch (e) {
        console.log(`[${this.module}] Error at [getPools]`)
        console.log(e)
        return null
      }
    }

    @Action
    async getPoolsRaw() {
      const method = "get/pools/raw"
      try {
        const response = await fetch(`${gatewayUrl}/${method}`, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
        })
        return await response.json()
      } catch (e) {
        console.log(`[${this.module}] Error at [${method}]`)
        console.log(e)
        return null
      }
    }
  }
}
