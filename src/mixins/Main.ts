import Vue from "vue";
import Component from "vue-class-component";

@Component
export default class MainMixin extends Vue {
  isPhone = false
  isTablet = false
  isDesktop = false
  isWide = false
  isLarge = false
  root = document.getElementsByTagName("html")[0]

  formatNumber(num: number, separator = ",") {
    return num
      .toString()
      .replace("-", "")
      .replace(/^[+-]?\d+/, int => int.replace(/(\d)(?=(\d{3})+$)/g, `$1${separator}`))
  }

  checkScreen() {
    const w = window.innerWidth
    this.isPhone = w < 769
    this.isTablet = w < 1024 && w > 768
    this.isDesktop = w < 1216 && w > 1023
    this.isWide = w > 1215 && w < 1408
    this.isLarge = w > 1407
  }

  mounted() {
    this.checkScreen()
		window.addEventListener("resize", this.checkScreen)

    this.root = document.getElementsByTagName("html")[0]
  }
}