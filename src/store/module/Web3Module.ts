import { VuexModule, Module, Mutation, Action } from "vuex-module-decorators"
import { ProviderController, ProviderControllerOptions } from "./../web3connector/ProviderController" 
import { provider } from "web3-core"
import { Contract } from "web3-eth-contract"
import { poolAbi, poolManagerAbi, tokenAbi, xpiAbi } from "./constants"
import Web3 from "web3"

function isLocalhost() {
  return false// window.location.href.match(/localhost/) !== null
}

export namespace Web3Module {
  export interface EventEmitter {
    on: (event: string, ...data: any) => void
  }

  export interface IProvider extends EventEmitter {
    chainId: string
    isMetaMask: boolean
  }

  export type SignatureData = {
    signature: string
    account: string
  }

  @Module({ namespaced: true })
  export class Web3Module extends VuexModule {
    $options: ProviderControllerOptions = {
      disableInjectedProvider: false,
      cacheProvider: true,
      providerOptions: {
        walletconnect: {
          rpc: {
            56: "https://bsc-dataseed.binance.org/",
          },
          chainId: 56,
        },
      },
    }
    $signatures: SignatureData[] =
      JSON.parse(String(localStorage.getItem("signature"))) === null ? [] : JSON.parse(String(localStorage.getItem("signature")))
    private providerController = new ProviderController(this.$options)
    $provider: IProvider = {} as IProvider
    $providerId: string | null = localStorage.getItem("providerId")
    $account: string | null = localStorage.getItem("account") as string
    $chainId: number | null = !localStorage.getItem("chainId") ? null : parseInt(localStorage.getItem("chainId") as string)
    $web3: Web3 = {} as Web3
    $resetCallback: Function = () => {}
    $accountChangedHandler: Function = () => {}
    $chainChangedHandler: Function = () => {}

    private tokenAddress = "0x7dAcc2327528A99aa1De0C1F757539A9A2380c04".toLowerCase()
    private airdropTokenAddress =  "0x0B801cceeCE698E6e5E82c4c53b3eE6068D23f0c".toLowerCase()
    private poolManagerAddress = "0xF7C3A8601A1048f6b5De10B00Aa1ef0E973d370F".toLowerCase()

    $token: Contract = {} as Contract
    $airdropToken: Contract = {} as Contract
    $poolManager: Contract = {} as Contract
    $poolManagerOwner: string = "0x" + "0".repeat(40)

    $pools: Array<Contract> = []

    get account() {
      return this.$account
    }

    get chainId() {
      return this.$chainId
    }

    get provider() {
      return this.$provider
    }

    get providerOptions() {
      return this.$options
    }

    get web3() {
      return this.$web3
    }

    get targetChainId() {
      return isLocalhost() ? 31337 : 56
    }

    get hasStoredData() {
      return this.$account !== null && this.$chainId !== null && this.$chainId == this.targetChainId
    }

    get isAuthorized() {
      return !!this.$poolManager?.methods
    }

    get token() {
      return this.$token
    }

    get airdropToken() {
      return this.$airdropToken
    }

    get poolManager() {
      return this.$poolManager
    }

    get poolManagerOwner() {
      return this.$poolManagerOwner
    }

    get pools() {
      return this.$pools
    }

    get accountChangedHandler() {
      return this.$accountChangedHandler
    }

    get chainChangedHandler() {
      return this.$chainChangedHandler
    }

    get signatures() {
      return this.$signatures
    }

    get providerId() {
      return this.$providerId
    }

    @Mutation
    setProviderId(id: string) {
      this.$providerId = id
      console.log(`PROVIDER ID: ${this.$providerId}`)
      localStorage.setItem("providerId", id)
    }

    @Mutation
    setSignature(signatureData: SignatureData) {
      const existed = this.$signatures.find(
        (s) => s.account === signatureData.account && s.signature === signatureData.signature
      )
      if (existed) return
      this.$signatures.push(signatureData)
      localStorage.setItem("signature", JSON.stringify(this.$signatures))
    }

    @Mutation
    setAccount(account: string) {
      this.$account = account
      if (account !== null) {
        localStorage.setItem("account", account)
      } else {
        localStorage.removeItem("account")
      }
    }

    @Mutation
    setChainId(chainId: number) {
      this.$chainId = chainId
      if (chainId !== null) {
        localStorage.setItem("chainId", String(chainId))
      } else {
        localStorage.removeItem("chainId")
      }
    }

    @Mutation
    setProvider(provider: IProvider) {
      this.$provider = provider
    }

    @Mutation
    setWeb3(web3: Web3) {
      this.$web3 = web3
    }

    @Mutation
    setResetCallback(cb: Function) {
      this.$resetCallback = cb
    }

    @Mutation
    addPool(pool: Contract) {
      this.$pools.push(pool)
    }

    @Mutation
    setBaseContracts(data: { token: Contract; airdropToken: Contract; poolManager: Contract }) {
      this.$token = data.token
      this.$airdropToken = data.airdropToken
      this.$poolManager = data.poolManager
    }

    @Mutation
    setPoolManagerOwner(owner: string) {
      this.$poolManagerOwner = owner.toLowerCase()
    }

    @Mutation
    setAccountChangedHandler(fn: Function) {
      this.$accountChangedHandler = fn
    }

    @Mutation
    setChainChangedHandler(fn: Function) {
      return (this.$chainChangedHandler = fn)
    }

    @Action
    async waitConnection(retries = 10) {
      if (retries === 0) return false
      await new Promise((resolve) => setTimeout(resolve, 500))
      if (!this.context.getters["isAuthorized"]) {
        return await this.context.dispatch("waitConnection", retries - 1)
      }
      return true
    }

    @Action
    async initalizeBaseContracts() {
      const baseContracts = {
        token: new this.web3.eth.Contract(tokenAbi, this.tokenAddress),
        airdropToken: new this.web3.eth.Contract(tokenAbi, this.airdropTokenAddress),
        poolManager: new this.web3.eth.Contract(poolManagerAbi, this.poolManagerAddress),
      }

      const owner = await baseContracts.poolManager.methods.owner().call()

      this.context.commit("setBaseContracts", baseContracts)
      this.context.commit("setPoolManagerOwner", owner)
    }

    @Action
    initalizePool(address: string) {
      const pool = new this.web3.eth.Contract(poolAbi, address)
      this.context.commit("addPool", pool)
      console.log(`Pool ${address} succefully initalized`)
    }

    @Action
    getPoolInstance(address: string) {
      return new this.web3.eth.Contract(poolAbi, address)
    }

    @Action
    getXPiInstance(address: string) {
      return new this.web3.eth.Contract(xpiAbi, address)
    }

    @Action
    getCustomTokenInstance(address: string) {
      return new this.web3.eth.Contract(tokenAbi, address)
    }

    @Action
    async changeSignature(web3: Web3 = this.context.getters["web3"]) {
      const accounts = await web3.eth.getAccounts()
      const signature = await web3.eth.personal.sign(
        "This signature request is for the sole purpose of identifying you as the owner of the wallet.",
        accounts[0],
        "123"
      )
      this.context.commit("setSignature", { account: accounts[0], signature })
    }

    @Action
    async connect(data: {
      id: string
      onAccountChangedCallback: Function
      onChainIdChangedCallback: Function
      onConnectCallback: Function
    }) {
      const providerId = data.id
      console.log(`received id to connect: ${data.id}`)
      this.providerController.on("connect", async (providerEvent) => {
        this.context.commit("setProvider", (providerEvent as CustomEvent).detail)
        console.log(this.provider)
        const web3 = new Web3(this.provider as unknown as provider)

        const accounts = await web3.eth.getAccounts()
        console.log(`account: ${accounts[0]}`)

        const onAccountChange = async (accounts: Array<string>) => {
          if (accounts.length > 0) {
            console.log(`Account changed to: ${accounts[0]}`)
            this.context.commit("setAccount", accounts[0])

            data.onAccountChangedCallback(accounts[0])
            return
          }
          await this.context.dispatch("reset")
        }

        const onChainChanged = async (chainId: string) => {
          this.context.commit("setChainId", parseInt(chainId))
          console.log(`Chain changed to: ${this.chainId}`)
          data.onChainIdChangedCallback(this.$chainId)
        }

        if (parseInt(this.provider.chainId) !== this.targetChainId) {
          return onChainChanged(this.provider.chainId)
        }

        console.log(this.provider)
        this.context.commit("setChainId", parseInt(this.provider.chainId))
        this.context.commit("setWeb3", web3)

        this.context.commit("setAccountChangedHandler", onAccountChange.bind(this))

        this.provider.on("accountsChanged", this.context.getters["accountChangedHandler"])

        this.context.commit("setChainChangedHandler", onChainChanged.bind(this))

        this.provider.on("chainChanged", this.context.getters["chainChangedHandler"])

        this.context.commit("setAccount", accounts[0])

        if (this.provider.isMetaMask) {
          this.context.commit("setProviderId", "injected")
        } else {
          this.context.commit("setProviderId", "walletconnect")
        }

        await this.context.dispatch("initalizeBaseContracts")

        data.onConnectCallback()
      })

      this.providerController.on("error", async (errorEvent) => {
        console.log(errorEvent)
      })

      await this.providerController.connectTo(data.id)
    }

    @Action
    onReset(cb: Function) {
      this.context.commit("setResetCallback", cb)
    }

    @Action
    async reset(preventRedirect = false) {
      console.log(`Reset web3`)
      try {
        await this.providerController.clearCachedProvider()
      } catch (e) {
        console.log(`Error while web3 exit`)
        console.error(e)
      }
      this.providerController.removeEvent("connect")
      this.providerController.removeEvent("error")
      this.context.commit("setAccount", null)
      this.context.commit("setChainId", null)

      this.context.getters["provider"].removeListener("accountsChanged", this.context.getters["accountChangedHandler"])
      this.context.getters["provider"].removeListener("chainChanged", this.context.getters["chainChangedHandler"])

      this.context.commit("setProvider", undefined)
      this.context.commit("setAccountChangedHandler", undefined)
      this.context.commit("setWeb3", undefined)
      const baseContracts = {
        token: undefined,
        airdropToken: undefined,
        poolManager: undefined,
      }
      this.context.commit("setBaseContracts", baseContracts)
      this.$resetCallback(preventRedirect)
    }
  }
}
