import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

import { loadLanguageAsync } from "@/i18n"

const routes = [
  {
    path: "/",
    name: "Wrap",
    component: () => import("@/views/Wrap.vue"),
    redirect: { name: "Homepage" },
    beforeEnter: (to, from, next) => {
      if (localStorage.getItem("i18n"))
        return loadLanguageAsync(localStorage.getItem("i18n")).then(() => next())

      const browserLang = navigator.language.split('-')[0]
      loadLanguageAsync(browserLang || "en").then(() => next())
    },
    children: [
      // {
      //   path: "*",
      //   name: "Stub",
      //   component: () => import("@/views/Stub.vue")
      // },
      {
        path: "",
        name: "Homepage",
        component: () => import("@/views/Homepage.vue")
      },
      {
        path: "buy-tokens",
        name: "BuyTokens",
        component: () => import("@/views/Homepage.vue")
      },
      {
        path: "login",
        name: "Login",
        component: () => import("@/views/Login.vue")
      },
      {
        path: "profile",
        name: "Profile",
        component: () => import("@/views/Profile.vue")
      },
      {
        path: "*",
        name: "404",
        component: () => import("@/views/404.vue")
      }
    ]
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  scrollBehavior(to, from, savedPosition) {
		if (to.matched.find(m => m.meta.disableScroll)) return

		if (to.hash) {
			return new Promise(resolve => {
				setTimeout(() => {
					resolve({ selector: to.hash, behavior: 'smooth' })
				}, 500)
			})
		} 
		
		if (savedPosition) {
			return new Promise(resolve => {
				setTimeout(() => {
					resolve({ ...savedPosition, behavior: 'smooth' })
				}, 500)
			})
		} else {
			return { x: 0, y: 0, behavior: 'smooth' }
		}
	},
  routes
})

export default router
