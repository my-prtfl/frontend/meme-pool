import Vue, { VueConstructor } from "vue"
import Web3Connector from "./components/List.vue"

export default {
  install(Vue: VueConstructor<Vue>) {
    Vue.component("web3connector", Web3Connector)
  },
}
