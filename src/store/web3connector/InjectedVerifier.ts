import { provider as Web3Provider } from "web3-core"
import * as env from "detect-browser"
import * as providers from "./providers"

export type CheckResult = {
  injectedAvailable?: boolean
  checks: { [checkKey: string]: boolean }
}

export class InjectedVerifier {
  public injected: Array<providers.InjectedProvider> = []

  constructor() {
    Object.keys(providers).map((providerKey) => {
      const castedKey = providerKey as keyof typeof providers
      if (providers[castedKey].id === "injected") this.injected.push(providers[castedKey] as providers.InjectedProvider)
    })
  }

  verify(checkKey: string) {
    return window.ethereum
      ? window.ethereum[checkKey]
      : window.web3 &&
          window.web3.currentProvider &&
          (window.web3.currentProvider as unknown as { [key: string]: boolean })[checkKey]
  }

  check() {
    const result: CheckResult = {
      injectedAvailable: !!window.ethereum || !!window.web3,
      checks: {
        [providers.fallback.check]: true,
      },
    }

    if (result.injectedAvailable) {
      this.injected.map((provider) => {
        const isAvailable = this.verify(provider.check)
        if (isAvailable) {
          result.checks[provider.check] = true
          delete result.checks[providers.fallback.check]
        }
      })
      const browser = env.detect()

      if (browser && browser.name === "opera") {
        result.checks[providers[browser.name].check] = true
        delete result.checks[providers.fallback.check]
      }
    }

    return result
  }

  private filterProviderChecks(checks: Array<string>) {
    if (!!checks && checks.length) {
      if (checks.length > 1) {
        if (
          checks[0] === providers.metamask.check
          //|| checks[0] === this.injected?.cipher.check
        ) {
          return checks[1]
        }
      }
      return checks[0]
    }
    return providers.fallback.check
  }

  getInjectedProvider() {
    const injectedProviders = this.check()
    if (injectedProviders.injectedAvailable) {
      const checks = Object.keys(injectedProviders.checks)
      return this.injected.find((provider) => provider.check === this.filterProviderChecks(checks))
    }
  }
}
