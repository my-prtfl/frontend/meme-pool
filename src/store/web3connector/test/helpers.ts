const providers = require("./providers")
const injected = require("./injected")
const chains = require("./chains")

Object.keys(injected).map(key => {
  providers[key] = injected[key]
})


// const window = {
//   web3: {
//     currentProvider: {
//       isMetaMask: true
//     }
//   }
// }

const env = {
  detect: () => ({ name: "chrome" })
}

function verifyInjectedProvider(check) {
  return window.ethereum
    ? window.ethereum[check]
    : window.web3 &&
    window.web3.currentProvider &&
    window.web3.currentProvider[check];
}

function checkInjectedProviders() {
  const result = {
    injectedAvailable: !!window.ethereum || !!window.web3
  };
  if (result.injectedAvailable) {
    let fallbackProvider = true;
    Object.values(injected).forEach((provider: any) => {
      const isAvailable = verifyInjectedProvider(provider.check);
      if (isAvailable) {
        result[provider.check] = true;
        fallbackProvider = false;
      }
    });

    const browser = env.detect();

    if (browser && browser.name === "opera") {
      result[injected.OPERA.check] = true;
      fallbackProvider = false;
    }

    if (fallbackProvider) {
      result[injected.FALLBACK.check] = true;
    }
  }

  return result;
}

//console.log(checkInjectedProviders())

function filterMatches(array, condition, fallback) {
  let result = fallback;
  const matches = array.filter(condition);

  if (!!matches && matches.length) {
    result = matches[0];
  }

  return result;
}

function filterProviders(param, value) {
  if (!value) return providers.FALLBACK;
  const match = filterMatches(
    Object.values(providers),
    x => x[param] === value,
    providers.FALLBACK
  );
  return match || providers.FALLBACK;
}

function filterProviderChecks(checks) {
  if (!!checks && checks.length) {
    if (checks.length > 1) {
      if (
        checks[0] === injected.METAMASK.check ||
        checks[0] === injected.CIPHER.check
      ) {
        return checks[1];
      }
    }
    return checks[0];
  }
  return providers.FALLBACK.check;
}

function getProviderInfoFromChecksArray(checks) {
  const check = filterProviderChecks(checks);
  return filterProviders("check", check);
}

function getInjectedProvider() {
  let result = null;

  const injectedProviders: any = checkInjectedProviders();

  if (injectedProviders.injectedAvailable) {
    delete injectedProviders.injectedAvailable;
    const checks = Object.keys(injectedProviders);
    result = getProviderInfoFromChecksArray(checks);
  }
  return result;
}

console.log(getInjectedProvider())

