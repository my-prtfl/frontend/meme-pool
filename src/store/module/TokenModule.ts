import { VuexModule, Module, Mutation, Action } from "vuex-module-decorators";
import { gatewayUrl } from "./constants";

export namespace TokenModule {
  @Module({ namespaced: true })
  export class TokenModule extends VuexModule {
    private module = "TokenModule";

    price: number | string = 0
    interval: number | null = null

    @Action
    async getTotalSupply() {
      const method = "get/totalSupply";
      try {
        const response = await fetch(`${gatewayUrl}/${method}`, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
        });
        return await response.json();
      } catch (e) {
        console.log(`[${this.module}] Error at [getTotalSupply]`);
        console.log(e);
        return null;
      }
    }

    @Mutation
    setInterval(fn: Function) {
      this.interval = setInterval(fn, 10 * 1000)
    }

    @Mutation
    setPrice(price: number | string) {
      this.price = price
    }

    @Action({ commit: 'setInterval' })
    getPrice() {
      if (!this.interval)
        return this.context.dispatch("fetchPrice")
      // const method = "price";
      // try {
      //   const response = await fetch(`${gatewayUrl}/${method}`, {
      //     method: "POST",
      //     headers: {
      //       "Content-Type": "application/json",
      //     },
      //   });
      //   return await response.json();
      // } catch (e) {
      //   console.log(`[${this.module}] Error at [${method}]`);
      //   console.log(e);
      //   return null;
      // }
    }

    @Action({ commit: 'setPrice' })
    async fetchPrice() {
      const method = "price";
      try {
        const response = await fetch(`${gatewayUrl}/${method}`, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
        });
        const { price } = await response.json();
        return price
      } catch (e) {
        console.log(`[${this.module}] Error at [${method}]`);
        console.log(e);
        return this.price;
      }
    }
  }
}
