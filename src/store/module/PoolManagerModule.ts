import { VuexModule, Module, Mutation, Action } from "vuex-module-decorators";
import { gatewayUrl } from "./constants";
import { RoleRegistry, Stake } from "./sharedTypes";

export namespace PoolManagerModule {
  export type PoolManagerTeamWallet = {
    id: number;
    index: number;
    address: string;
  };

  @Module({ namespaced: true })
  export class PoolManager extends VuexModule {
    private module = "PoolModule";

    $teamWallets: Array<PoolManagerTeamWallet> = [];
    $roles: Array<RoleRegistry> = [];

    get teamWallets() {
      return this.$teamWallets;
    }

    get roles() {
      return this.$roles;
    }

    @Mutation
    setTeamWallets(wallets: Array<PoolManagerTeamWallet>) {
      this.$teamWallets = wallets;
    }

    @Mutation
    setRoles(roles: Array<RoleRegistry>) {
      this.$roles = roles;
    }

    @Action
    async getTeamWallets() {
      const method = "get/team/wallets";
      try {
        const response = await fetch(`${gatewayUrl}/${method}`, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
        });
        this.context.commit("setTeamWallets", await response.json());
      } catch (e) {
        console.log(`[${this.module}] Error at [getTeamWallets]`);
        console.log(e);
        return null;
      }
    }

    @Action
    async getRolesRegistry() {
      const method = "get/roles/registry";
      try {
        const response = await fetch(`${gatewayUrl}/${method}`, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
        });
        this.context.commit("setRoles", await response.json());
      } catch (e) {
        console.log(`[${this.module}] Error at [getRolesRegistry]`);
        console.log(e);
        return null;
      }
    }

    @Action
    async getCurrentStakeProfitPercent() {
      const method = "get/current/stake/profit/percent";
      try {
        const response = await fetch(`${gatewayUrl}/${method}`, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
        });
        return await response.json();
      } catch (e) {
        console.log(`[${this.module}] Error at [${method}]`);
        console.log(e);
        return null;
      }
    }
  }
}
