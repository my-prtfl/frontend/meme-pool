export type Stake = {
  id: number
  poolId: number
  userId: number
  hash: string
  stakeIndex: number
  body: string
  referer: string
  createdAt: Date
  expiresIn: Date

  claims?: Array<Claim>
}

export type Claim = {
  id: number
  stakeId: number
  hash: string
  amount: string
  timestamp: Date
}

export type RoleRegistry = {
  id: number
  name: string
  contractPermissions: Array<string>
  appPermissions: Array<string>
}

export type SignatureData = {
  signature: string
  account: string
}
