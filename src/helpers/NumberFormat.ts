import BigNumber from "bignumber.js";

export class NumberFormat {
  static format(n: any) {
    const billion = 100_000_000_000;
    const bn = new BigNumber(n);
    if (bn.isGreaterThan(billion)) {
      return bn.dividedBy(billion).toFormat(0, 0, { groupSeparator: " ", groupSize: 3 }) + " B";
    }
    if (bn.isLessThan(1)) {
      const significant = NumberFormat.getSignificantPosition(bn);
      return bn.isEqualTo(0) ? "0" :  bn.toFixed(significant + 10, 1);
    }
    const output = bn.toFormat(3, 1, { groupSeparator: " ", groupSize: 3, decimalSeparator: "." });
    if (output.split(".")[1] === "000") return output.split(".")[0]
    return output
  }

  static getSignificantPosition(n: BigNumber | string | number) {
    const bn = new BigNumber(n).abs();
    const splited = bn.toFormat().split("");
    splited.shift();
    splited.shift();
    const index = splited.findIndex((s) => parseInt(s) !== 0);
    return index === -1 ? 0 : index;
  }
}
