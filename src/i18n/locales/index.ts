const locales = require.context(`./`, true, /[A-Za-z0-9-_,\s]+\.json$/i, "lazy")

export default lang => {
	return new Promise(async ( resolve, reject ) => {
		const messages = {}

		await Promise.all(
			locales.keys().map(async path => {
				if (path.includes(`./${lang}/`)) {
					const reg = new RegExp(`./${lang}/(.*).json`)
					
					const [ base, name ]: any = path.match(reg)
					
					messages[name] = await locales(path)
				}
			})
		)
		
		resolve(messages)
	})
}