import Vue from "vue"
import VueI18n from "vue-i18n"

import getLocale from "./locales"

Vue.use(VueI18n)

const pluralizationRules = {
  'ru': (choice, choicesLength) => {
      if (choice === 0) return 0
      const teen = choice > 10 && choice < 20
      const endsWithOne = choice % 10 === 1
      if (choicesLength < 4) return (!teen && endsWithOne) ? 1 : 2
      if (!teen && endsWithOne) return 1
      if (!teen && choice % 10 >= 2 && choice % 10 <= 4) return 2
      return (choicesLength < 4) ? 2 : 3
  }
}

const createI18n = () => new VueI18n({ pluralizationRules })

const i18n = createI18n()

const loadedLanguages: string[] = []

const setI18nLanguage = lang => {
  i18n.locale = lang
  document.querySelector('html')?.setAttribute('lang', lang)
  return lang
}

const loadLanguageAsync = lang => {
  localStorage.setItem("i18n", lang)

  if (i18n.availableLocales.indexOf(lang) > -1)
    return Promise.resolve(setI18nLanguage(lang))
	
  return getLocale(lang).then((messages: any) => {
		i18n.setLocaleMessage(lang, messages)
		loadedLanguages.push(lang)
		return setI18nLanguage(lang)
	})
}

export { loadLanguageAsync }
export default i18n