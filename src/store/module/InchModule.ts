import { VuexModule, Module, Mutation, Action } from "vuex-module-decorators";
import { RoleRegistry, Stake } from "./sharedTypes";

export namespace InchModule {
  export type InchToken = {
    symbol: string;
    name: string;
    address: string;
    decimals: number;
    logoURI: string;
  };

  export type AllowanceRequest = {
    tokenAddress: string;
    walletAddress: string;
  };

  export type QuoteRequest = {
    fromTokenAddress: string;
    toTokenAddress: string;
    amount: string;

    protocols?: string;
    fee?: string; //Min: 0; max: 3; Max: 0; max: 3; default: 0; !should be the same for quote and swap!

    gasLimit?: string;
    connectorTokens?: string; //max: 5; !should be the same for quote and swap!

    complexityLevel?: string; //min: 0; max: 3; default: 2; !should be the same for quote and swap!

    mainRouteParts?: string; //default: 10; max: 50 !should be the same for quote and swap!

    parts?: string; //default: 50; max: 100!should be the same for quote and swap!

    gasPrice?: string;
  };

  export type QuoteResponse = {
    fromToken: InchToken;
    toToken: InchToken;
    toTokenAmount: string; //"string"
    fromTokenAmount: string; //"string"
    protocols: {
      name: string; //"string"
      part: number; //0
      fromTokenAddress: string; //"string"
      toTokenAddress: string; //"string"
    }[];
    estimatedGas: number; //0
  };

  export type ApproveTransactionDataRequest = {
    tokenAddress: string;
    amount: string;
  };

  export type ApproveTransactionDataResponse = {
    data: string; //"string"
    gasPrice: string; //"string"
    to: string; //"0x6b175474e89094c44da98b954eedeac495271d0f"
    value: string; //"string"
  };

  export type SwapRequest = {
    fromTokenAddress: string;
    toTokenAddress: string;
    amount: string;
    fromAddress: string;
    slippage: number; //min: 0; max: 50;Example : 1
    protocols?: string; //
    destReceiver?: string;
    referrerAddress?: string;
    fee?: string; //Min: 0; max: 3; Max: 0; max: 3; default: 0; !should be the same for quote and swap!

    gasPrice?: string; //default: fast from network

    disableEstimate?: boolean;
    permit?: string;

    burnChi?: boolean;
    allowPartialFill?: boolean;
    parts?: string;
    mainRouteParts?: string; //default: 10; max: 50 !should be the same for quote and swap!

    connectorTokens?: string; //max: 5; !should be the same for quote and swap!

    complexityLevel?: string; //min: 0; max: 3; default: 2; !should be the same for quote and swap!

    gasLimit?: string;
  };

  export type SwapResponse = {
    fromToken: InchToken;
    toToken: InchToken;
    toTokenAmount: string;
    fromTokenAmount: string;
    protocols: string[];
    tx: {
      from: string;
      to: string;
      data: string;
      value: string;
      gasPrice: string;
      gas: string;
    };
  };

  @Module({ namespaced: true })
  export class InchModule extends VuexModule {
    private module = "InchModule";
    private gatewayUrl = "https://api.1inch.io/v4.0/56";

    $apiStatus = false;
    $tokens: InchToken[] = [];
    $tokensTable: { [name: string]: InchToken } = {};
    $inchRouterAddress: string = "0x" + "0".repeat(40);

    get apiStatus() {
      return this.$apiStatus;
    }

    get tokens() {
      return [
        ...this.$tokens,
        {
					symbol: "IBT",
					name: "International Blockchain Technology",
					decimals: 18,
					address: "0x7dacc2327528a99aa1de0c1f757539a9a2380c04",
					logoURI: "/IBT.png"
				}
      ];
    }

    get tokensTable() {
      return this.$tokensTable;
    }

    get inchRouterAddress() {
      return this.$inchRouterAddress;
    }

    @Mutation
    setInchRouterAddress(data: { address: string }) {
      return (this.$inchRouterAddress = data.address);
    }

    @Mutation
    setTokens(tokens: { [address: string]: InchToken }) {
      this.$tokens = Object.values(tokens);
      this.$tokensTable = tokens;
    }

    @Mutation
    setApiStatus(status: boolean) {
      this.$apiStatus = status;
    }

    @Action
    async healthcheck() {
      const method = "healthcheck";
      try {
        const response = await fetch(`${this.gatewayUrl}/${method}`, {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
          },
        });
        if (response.status === 200) {
          return this.context.commit("setApiStatus", true);
        }
        this.context.commit("setApiStatus", false);
      } catch (e) {
        console.log(`[${this.module}] Error at [${method}]`);
        console.log(e);
        this.context.commit("setApiStatus", false);
        return null;
      }
    }

    @Action
    async getTokens() {
      const method = "tokens";
      try {
        const response = await fetch(`${this.gatewayUrl}/${method}`, {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
          },
        });
        return this.context.commit("setTokens", (await response.json()).tokens);
      } catch (e) {
        console.log(`[${this.module}] Error at [${method}]`);
        console.log(e);
        return null;
      }
    }

    @Action
    async getRouter() {
      const method = "approve/spender";
      try {
        const response = await fetch(`${this.gatewayUrl}/${method}`, {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
          },
        });
        this.context.commit("setInchRouterAddress", await response.json());
      } catch (e) {
        console.log(`[${this.module}] Error at [${method}]`);
        console.log(e);
        return null;
      }
    }

    @Action
    async getAllowance(data: AllowanceRequest) {
      const method = "approve/allowance";
      try {
        const params = new URLSearchParams(data  as unknown as Record<string, string>);
        const response = await fetch(`${this.gatewayUrl}/${method}?${params}`, {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
          },
        });
        return (await response.json()).allowance;
      } catch (e) {
        console.log(`[${this.module}] Error at [${method}]`);
        console.log(e);
        return null;
      }
    }

    @Action
    async getApproveTransactionData(data: ApproveTransactionDataRequest) {
      const method = "approve/transaction";
      try {
        const params = new URLSearchParams(data  as unknown as Record<string, string>);
        const response = await fetch(`${this.gatewayUrl}/${method}?${params}`, {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
          },
        });
        return await response.json();
      } catch (e) {
        console.log(`[${this.module}] Error at [${method}]`);
        console.log(e);
        return null;
      }
    }

    @Action
    async getQuote(data: QuoteRequest) {
      const method = "quote";
      try {
        const params = new URLSearchParams(data  as unknown as Record<string, string>);
        const response = await fetch(`${this.gatewayUrl}/${method}?${params}`, {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
          },
        });
        return await response.json();
      } catch (e) {
        console.log(`[${this.module}] Error at [${method}]`);
        console.log(e);
        return null;
      }
    }

    @Action
    async swap(data: SwapRequest) {
      const method = "swap";
      try {
        const params = new URLSearchParams(data as unknown as Record<string, string>);
        const response = await fetch(`${this.gatewayUrl}/${method}?${params}`, {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
          },
        });
        return await response.json();
      } catch (e) {
        console.log(`[${this.module}] Error at [${method}]`);
        console.log(e);
        return null;
      }
    }
  }
}
