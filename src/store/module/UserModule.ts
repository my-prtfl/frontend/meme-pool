import { VuexModule, Module, Mutation, Action } from "vuex-module-decorators";
import { gatewayUrl } from "./constants";
import { RoleRegistry, Stake } from "./sharedTypes";

export namespace UserModule {
  export type User = {
    id: number;
    address: string;

    stats?: UserStats;
    poolStats?: Array<UserPoolStats>;
    p2pTransfersReceiver?: Array<UserP2PTransfer>;
    p2pTransfersSender?: Array<UserP2PTransfer>;
    stakes?: Array<Stake>;
    burns?: Array<UserBurn>;
    poolXPiLevel?: PoolXPiUserLevel;
    referrals?: Array<Referral>;
    refererStats?: Array<RefererStats>;
    roles?: Array<PoolUserRole>;
    poolManagerRole: PoolManagerUserRole;
    referers?: Array<Referral>
  };

  export type UserStats = {
    userId: number;
    lastShowedStakeId: number;
    lastShowedClaimId: number;
    lastShowedP2PTransferId: number;
  };

  export type UserPoolStats = {
    poolId: number;
    userId: number;
    totalStakes: number;
    totalActiveStakes: number;
    totalStakesSum: string;
    totalActiveStakesSum: string;
    totalClaims: number;
    totalClaimsSum: string;
  };

  export type UserP2PTransfer = {
    id: number;
    fromId: number;
    toId: number;
    hash: string;
    from: string;
    to: string;
    value: string;
    timestamp: Date;
  };

  export type UserBurn = {
    id: number;
    userId: number;
    hash: string;
    value: string;
    timestamp: Date;
  };

  export type PoolXPiUserLevel = {
    id: number;
    userId: number;
    level: number;
  };

  export type Referral = {
    id: number;
    poolId: number;
    refererId: number;
    referralId: number;
    line: number;
    totalReward: string;
    registeredAt: Date;

    referral?: User;
  };

  export type RefererStats = {
    id: number;
    poolId: number;
    refererId: number;
    line: number;
    totalReferrals: number;
    totalStakes: number;
    totalActiveStakes: number;
    totalStakesSum: string;
    totalActiveStakesSum: string;
  };

  export type PoolUserRole = {
    id: number;
    poolId: number;
    userId: number;
    roleId: number;
  };

  export type PoolManagerUserRole = {
    id: number;
    userId: number;
    roleId: number;

    role?: RoleRegistry;
  };

  @Module({ namespaced: true })
  export class UserModule extends VuexModule {
    private module = "UserModule";
    $user: User = {} as User;
    $users: Array<User> = [];
    private usersTable: { [id: number]: number } = {};

    get user() {
      return this.$user;
    }

    get users() {
      return this.$users;
    }

    @Mutation
    setUser(user: User) {
      this.$user = user;
    }

    @Mutation
    setUsers(users: Array<User>) {
      this.$users = users;
    }

    @Action
    async getUsersCount() {
      const method = "get/users/count";
      try {
        const response = await fetch(`${gatewayUrl}/${method}`, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
        });
        return await response.json();
      } catch (e) {
        console.log(`[${this.module}] Error at [getUsersCount]`);
        console.log(e);
        return null;
      }
    }

    @Action
    async getUser(address: string) {
      const method = "get/user";
      try {
        const response = await fetch(`${gatewayUrl}/${method}`, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({ address }),
        });
        this.context.commit("setUser", await response.json());
      } catch (e) {
        console.log(`[${this.module}] Error at [getUser]`);
        console.log(e);
        return null;
      }
    }

    @Action
    async getAnyUser(address: string) {
      const method = "get/user";
      try {
        const response = await fetch(`${gatewayUrl}/${method}`, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({ address }),
        });
        return await response.json()
      } catch (e) {
        console.log(`[${this.module}] Error at [getUser]`);
        console.log(e);
        return null;
      }
    }

    @Action
    async getUsers() {
      const method = "get/users";
      try {
        const response = await fetch(`${gatewayUrl}/${method}`, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
        });
        this.context.commit("setUsers", await response.json());
      } catch (e) {
        console.log(`[${this.module}] Error at [getUser]`);
        console.log(e);
        return null;
      }
    }
  }
}
