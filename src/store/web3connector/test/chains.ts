module.exports = {
  1: {
    chainId: 1,
    chain: "ETH",
    network: "mainnet",
    networkId: 1,
  },
  4: {
    chainId: 4,
    chain: "ETH",
    network: "rinkeby",
    networkId: 4,
  },
  56: {
    chainId: 56,
    chain: "BSC",
    network: "binance",
    networkId: 56,
  },
  97: {
    chainId: 97,
    chain: "BSC-testnet",
    network: "binance",
    networkId: 97
  }
}