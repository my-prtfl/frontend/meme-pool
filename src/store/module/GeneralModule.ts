import { VuexModule, Module, Mutation, Action } from "vuex-module-decorators"
import { gatewayUrl } from "./constants"
import { RoleRegistry, Stake, SignatureData } from "./sharedTypes"

export namespace GeneralModule {
  export type Vacancy = {
    id: number
    createdAt: Date

    data: VacancyData[]
  }

  export type VacancyData = {
    id: number
    vacancyId: number
    lang: string
    title: string
    responsibilities: string
    requirements: string
  }

  @Module({ namespaced: true })
  export class GeneralModule extends VuexModule {
    private module = "GeneralModule"

    @Action
    async getVacancies() {
      const method = "get/vacancies"
      try {
        const response = await fetch(`${gatewayUrl}/${method}`, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
        })
        return await response.json()
      } catch (e) {
        console.log(`[${this.module}] Error at [${method}]`)
        console.log(e)
        return null
      }
    }
    @Action
    async addVacancy(data: { langs: VacancyData[] } & SignatureData) {
      const method = "add/vacancy"
      try {
        const response = await fetch(`${gatewayUrl}/${method}`, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(data),
        })
        return await response.json()
      } catch (e) {
        console.log(`[${this.module}] Error at [${method}]`)
        console.log(e)
        return null
      }
    }
    @Action
    async updateVacancy(data: { id: number; langs: VacancyData[] } & SignatureData) {
      const method = "update/vacancy"
      try {
        const response = await fetch(`${gatewayUrl}/${method}`, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(data),
        })
        return await response.json()
      } catch (e) {
        console.log(`[${this.module}] Error at [${method}]`)
        console.log(e)
        return null
      }
    }
    @Action
    async removeVacancy(data: { id: number } & SignatureData) {
      const method = "remove/vacancy"
      try {
        const response = await fetch(`${gatewayUrl}/${method}`, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(data),
        })
        return await response.json()
      } catch (e) {
        console.log(`[${this.module}] Error at [${method}]`)
        console.log(e)
        return null
      }
    }
  }
}
