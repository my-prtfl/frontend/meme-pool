import { connectToInjected } from "./injected"
import { connectToWalletConnect } from "./walletconnect"

export default {
  injected: connectToInjected,
  walletconnect: connectToWalletConnect
}