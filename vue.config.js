const { defineConfig } = require('@vue/cli-service')
const path = require("path")
const webpack = require("webpack")

module.exports = defineConfig({
  transpileDependencies: true,
  chainWebpack: config => {
    config.module.rules.delete("svg");
  },
  css: {
    loaderOptions: {
      scss: {
        additionalData: `
          @import "@/assets/scss/common/variables";
          @import "@/assets/scss/common/mixins";
        `
      }
    }
  },
  configureWebpack: {
    resolve: {
      alias: {
        "@": path.join(__dirname, "src"),
        "svg": path.join(__dirname, "src", "assets", "svg")
      },
      extensions: [".ts", ".js"],
      fallback: {
        "buffer": require.resolve("buffer"),
        "http": require.resolve("stream-http"),
        "https": require.resolve("https-browserify"),
        "crypto": require.resolve("crypto-browserify"),
        "stream": require.resolve("stream-browserify"),
        "os": require.resolve("os-browserify/browser"),
        "url": require.resolve("url"),
        "assert": require.resolve("assert")
      }
    },
    plugins: [
      new webpack.ProvidePlugin({
        Buffer: ['buffer', 'Buffer'],
      }),
      new webpack.ProvidePlugin({
        process: 'process/browser',
      }),
    ],
    module: {
      rules: [
        {
          test: /\.svg$/,
          loader: 'vue-svg-loader',
        }
      ]
    },
    optimization: {
      splitChunks: {
        chunks: 'all'
      }
    }
  },
  pwa: {
    name: "Memepool",
    themeColor: "#FF9700",
    msTileColor: "#0c0f14",
    appleMobileWebAppCapable: "yes",
    appleMobileWebAppStatusBarStyle: "black",
    manifestOptions: {
      short_name: "Memepool",
      name: "Memepool | Just memeing, just staking",
      theme_color: "#FF9700",
      background_color: "#0c0f14",
      start_url: "."
    }
  }
})
