import * as WalletConnectProvider from "@walletconnect/web3-provider"
import { IWalletConnectProviderOptions } from "@walletconnect/types"

export const connectToWalletConnect = (options?: IWalletConnectProviderOptions): Promise<WalletConnectProvider.default> => {
  return new Promise(async (resolve, reject) => {
    if (!options) options = {}
    options.bridge = options && options.bridge ? options.bridge : "https://bridge.walletconnect.org"
    options.qrcode = options && options.qrcode ? options.qrcode : true
    options.infuraId = options && options.infuraId ? options.infuraId : "9aa3d95b3bc440fa88ea12eaa4456161"
    options.rpc = options && options.rpc ? options.rpc : undefined
    options.chainId = options && options.chainId ? options.chainId : 1
    options.qrcodeModalOptions = options && options.qrcodeModalOptions ? options.qrcodeModalOptions : undefined

    const provider = new WalletConnectProvider.default(options)

    try {
      await provider.enable()
      resolve(provider)
    } catch (e) {
      reject(e)
    }
  })
}
