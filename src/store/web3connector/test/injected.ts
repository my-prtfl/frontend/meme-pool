module.exports.FALLBACK = {
  id: "injected",
  name: "Web3",
  type: "injected",
  check: "isWeb3",
}

module.exports.METAMASK = {
  id: "injected",
  name: "MetaMask",
  type: "injected",
  check: "isMetaMask",
}

module.exports.OPERA = {
  id: "injected",
  name: "Opera",
  type: "injected",
  check: "isOpera",
}
