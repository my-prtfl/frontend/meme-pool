import { provider as Web3Provider } from "web3-core"

export const connectToInjected: () => Promise<Web3Provider> = async () => {
  let provider: any = null
  if (typeof window.ethereum !== "undefined") {
    provider = window.ethereum
    try {
      await provider.request({ method: "eth_requestAccounts" })
    } catch(error) {
      throw new Error("User reject")
    }
  } else if (window.web3) {
    provider = window.web3.currentProvider
  } else {
    throw new Error("No Web3 Provider found")
  }
  return provider
}