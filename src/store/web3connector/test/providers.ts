module.exports.WALLETCONNECT = {
  id: "walletconnect",
  name: "WalletConnect",
  type: "qrcode",
  check: "isWalletConnect",
  package: {
    required: ["infuraId", "rpc"],
  },
}