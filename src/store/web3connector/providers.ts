import { provider as Web3Provider } from "web3-core"
import * as WalletConnectProvider from "@walletconnect/web3-provider"
import { IWalletConnectProviderOptions } from "@walletconnect/types"
import connectors from "./connectors"

export type ProviderType = "injected" | "web" | "qrcode"

export type Provider<T, U, Z> = {
  id: T
  name: string
  type: ProviderType
  check: string
  connector: (options?: Z) => Promise<U>
  package?: {
    required: Array<string>
  }
}

export type InjectedProvider = Provider<"injected", Web3Provider, undefined>

export const fallback: InjectedProvider = {
  id: "injected",
  name: "Web3",
  type: "injected",
  check: "isWeb3",
  get connector() {
    return connectors[this.id]
  },
}

export const metamask: InjectedProvider = {
  id: "injected",
  name: "MetaMask",
  type: "injected",
  check: "isMetaMask",
  get connector() {
    return connectors[this.id]
  },
}

export const opera: InjectedProvider = {
  id: "injected",
  name: "Opera",
  type: "injected",
  check: "isOpera",
  get connector() {
    return connectors[this.id]
  },
}

export const walletconnect: Provider<"walletconnect", WalletConnectProvider.default, IWalletConnectProviderOptions> = {
  id: "walletconnect",
  name: "WalletConnect",
  type: "qrcode",
  check: "isWalletConnect",
  get connector() {
    return connectors[this.id]
  },
}